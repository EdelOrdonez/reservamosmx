export interface ICity {
  city_name: string;
  city_slug: string;
  country: string;
  display: string;
  lat: string;
  long: string;
  slug: string;
  state: string;
}

export default ICity;
