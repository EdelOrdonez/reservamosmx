import { BasicResponse, Service } from "../Service/Service";
import axios from "axios";

// TODO: Remove any type
class OpenWeatherService extends Service<any> {
  protected name = "weather";
  protected OPEN_WEATHER_API_KEY = "a5a47c18197737e8eeca634cd6acb581"; // Todo: Move this to an env file

  // TODO: Remove any type
  async getWeather(lat: string, lon: string): Promise<any> {
    try {
      const response = await axios.get<BasicResponse<any>>(
        `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&appid=${this.OPEN_WEATHER_API_KEY}`
      );

      return response.data;
    } catch (error) {
      if (error.response) {
        throw new Error(error.response.data.message);
      } else {
        throw new Error(error.message);
      }
    }
  }
}

const service = new OpenWeatherService();
export default service;
