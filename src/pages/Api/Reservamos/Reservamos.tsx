import { Service } from "../Service/Service";
import axios from "axios";
import ICity from "./../../../Types/City";

// TODO: Remove any type
class ReservamosService extends Service<any> {
  protected name = "reservamos";

  async getCities(city_name: string): Promise<ICity[]> {
    try {
      const response = await axios.get<any>(
        `https://search.reservamos.mx/api/v2/places?q=${city_name}`
      );
      const cities = response.data.filter(
        (city: { result_type: string }) => city.result_type === "city"
      );
      return cities;
    } catch (error) {
      if (error.response) {
        throw new Error(error.response.data.message);
      } else {
        throw new Error(error.message);
      }
    }
  }
}

const service = new ReservamosService();
export default service;
