import React from "react";
import { Row, Col, Input } from "antd";
import "./SearchSection.scss";

const { Search } = Input;

interface ISearchInput {
  loading: boolean;
  onSearch: (value: string) => void;
}

const SearchInput: React.FC<ISearchInput> = ({ loading, onSearch }) => {
  return (
    <Row justify="center" className="searchInput">
      <Col span={20}>
        <Row justify="center" className="searchInput__wrapper">
          <Col span={24}>
            <h3>
              <span className="primary">FreeWeather!</span>
              <br />
              <span className="secondary">The ultimate</span>
              <br />
              <span className="secondary">Weather Page</span>
            </h3>
          </Col>
          <Col span={24} className="searchInput__input">
            <Search
              placeholder="Write the name fo a city or part of it"
              allowClear
              enterButton="Search"
              size="large"
              loading={loading}
              onSearch={onSearch}
            />
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

export default SearchInput;
