import React from "react";
import { Row, Col } from "antd";
import "./WeatherSection.scss";
import { If, Then } from "react-if";
import ICity from "../../../../Types/City";
import WeatherCard from "./../../atoms/WeatherCard/WeatherCard";

interface IWeatherSection {
  mainCityWeather: [];
  selectedCity: ICity | undefined; //TODO: Remove this undefined
}

const WeatherSection: React.FC<IWeatherSection> = ({
  mainCityWeather,
  selectedCity,
}) => {
  return (
    <Row
      id="weather"
      justify="center"
      className="weatherSection"
      gutter={[5, 5]}
    >
      <If condition={mainCityWeather.length}>
        <Then>
          <Col span={24} className="weatherSection__title">
            <h2>{selectedCity?.display} los siguientes 7 días</h2>
          </Col>
          {mainCityWeather.map((weather: any, index) => {
            return (
              <Col xs={20} md={10} lg={6}>
                <WeatherCard
                  key={index}
                  title={index}
                  min={weather.temp.min}
                  eve={weather.temp.eve}
                  max={weather.temp.max}
                  isHighest={weather.isHighest}
                />
              </Col>
            );
          })}
        </Then>
      </If>
    </Row>
  );
};

export default WeatherSection;
