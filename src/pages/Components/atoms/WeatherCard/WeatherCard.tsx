import React from "react";
import { Card, Row, Col } from "antd";
import "./WeatherCard.scss";
import { If, Then } from 'react-if';

interface IWeatherCard {
  title: number;
  min: number;
  max: number;
  eve: number;
  isHighest: boolean;
}

const titles = [
  "Hoy",
  "Mañana",
  "En 2 Días",
  "En 3 Días",
  "En 4 días",
  "En 5 días",
  "En 6 Días",
  "En 1 Semana",
];

const convertGrades = (kelvinGrades: number) => {
  const kelvinConversion = 273.15;
  return (kelvinGrades - kelvinConversion).toFixed(2);
};

const WeatherCard: React.FC<IWeatherCard> = ({ title, min, max, eve, isHighest }) => {
  console.log(isHighest)
  return (
    <Card title={titles[title]} bordered={false} className="weatherCard">
      <If condition={isHighest}>
        <Then>
        <span>ESTE ES EL MAS ALTO</span>
        </Then>
      </If>
      <Row justify="center">
        <Col xs={20} md={8}>
          <h4>Min:</h4>
          <h5>{convertGrades(min)} °C</h5>
          <h6>{min} °K </h6>
        </Col>
        <Col xs={20} md={8}>
          <h4>Eve:</h4>
          <h5>{convertGrades(eve)} °C</h5>
          <h6>{eve} °K </h6>
        </Col>
        <Col xs={20} md={8}>
          <h4>Max:</h4>
          <h5>{convertGrades(max)} °C</h5>
          <h6>{max} °K </h6>
        </Col>
      </Row>
    </Card>
  );
};

export default WeatherCard;
