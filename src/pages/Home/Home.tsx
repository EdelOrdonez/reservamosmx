import React, { useEffect, useState } from "react";
import "./Home.scss";
import { notification } from "antd";
import OpenWeatherService from "../Api/OpenWeather";
import ReservamosService from "../Api/Reservamos";
import ICity from "./../../Types/City";
import SearchSection from "../Components/molecules/SearchSection/SearchSection";
import WeatherSection from "../Components/molecules/WeatherSection/WeatherSection";

const Home: React.FC<any> = () => {
  const [cities, setCities] = useState<ICity[]>([]);
  const [selectedCity, setSelectedCity] = useState<ICity>();
  const [loading, setLoading] = useState(false);
  const [mainCityWeather, setMainCityWeather] = useState<any>([]);
  const [weatherTop, setWeatherTop] = useState<number>(0);

  const getWeather = async (lat: string, lon: string) => {
    let weather = await OpenWeatherService.getWeather(lat, lon);
    return weather;
  };

  const getMainCityWeather = async (res: any) => {
    let mainCity: ICity = res[0];
    setSelectedCity(mainCity);
    if (mainCity) {
      let mainWeather = await getWeather(mainCity.lat, mainCity.long);
      const modified_weather = getHighestH(mainWeather?.daily)
      setMainCityWeather(modified_weather)
    }
  };

  const getHighestH = (weather: any) => {
    let weather_handler = weather;
    let highest_humidity = 0;
    let highest_index = 0;
    weather.forEach((element: any, index: any) => {
      if(element.humidity > highest_humidity) {
        highest_index = index;
        highest_humidity = element.humidity;
      }
    });
    weather_handler[highest_index].isHighest = true
    return weather_handler;
  }

  const getCities = async (text: string, scroll: boolean) => {
    try {
      setLoading(true);
      await ReservamosService.getCities(text).then((res: ICity[]) => {
        getMainCityWeather(res);
        setCities(res);
      });
    } catch (error) {
      openNotification(true, "Ha ocurrido algo inesperado :(");
    } finally {
      setLoading(false);
      if (scroll) {
        window.scrollTo(0, weatherTop); // Super basic smooth scroll
      }
    }
  };

  const onSearch = (value: string, scroll: boolean) => {
    getCities(value, scroll);
  };

  const openNotification = (error: boolean, message: string) => {
    notification.open({
      message: message,
      style: {
        backgroundColor: !error ? "green" : "red",
        color: "#ffffff",
      },
    });
  };

  // This function can be replaced with an animation library as GSAP, Scrolltrigger, etc.
  const getToffsetTop = () => {
    let testDiv = document.getElementById("weather");
    setWeatherTop(testDiv?.offsetTop!);
  };

  useEffect(() => {
    getToffsetTop();
    getCities("Merida", false);
  }, []);

  return (
    <div className="home">
      <SearchSection
        loading={loading}
        onSearch={(data) => onSearch(data, true)}
      />
      <WeatherSection
        mainCityWeather={mainCityWeather ?? []}
        selectedCity={selectedCity}
      />
    </div>
  );
};

export default Home;
